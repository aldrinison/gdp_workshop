import gdp
import random
import json
import os

print("\n##### START %s #####\n" % os.path.basename(__file__))

# get GCL and PEM filenames from config.json
config = json.load(open("pyconfig.json"))
GCL = config["GCL"]
encrypted_pkey = config["encrypted_pkey"]
unencrypted_pkey = config["unencrypted_pkey"]

# initialize gdp package
print "Initializing gdp..."
gdp.gdp_init()

# converts the GCL common name to its 256-bit name
gcl_name = gdp.GDP_NAME(GCL)

# only needed when writing to GCL
# PEM file with unencrypted private key can be generated using
# openssl pkey -in <encrypted>.pem -out <unencrypted>.pem
# PEM passphrase is needed when using encrypted private key
skey = gdp.EP_CRYPTO_KEY(filename=unencrypted_pkey, keyform=gdp.EP_CRYPTO_KEYFORM_PEM, flags=gdp.EP_CRYPTO_F_SECRET)

# open the GCL for appending
gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_AO, {"skey": skey})

# appends random value between 1 and 1000 to the GCL
print("Appending random integer between 1 and 1000 to GCL %s..." % GCL)
data = random.randint(100, 999)
try:
    # synchronous append which blocks execution
    # for non-blocking implementation, asynchronous append 
    #   can be done with the append_async method
    gcl_handle.append({"data": str(data)})
    print "Appended data:", str(data)
    print("\n###### END %s ######\n" % os.path.basename(__file__))
except:
    raise
