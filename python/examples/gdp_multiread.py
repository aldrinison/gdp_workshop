import gdp
import sys
import json
import os

print("\n##### START %s #####\n" % os.path.basename(__file__))

# parse command line arguments for recno and num
try:
    recno = int(sys.argv[1])
    num = int(sys.argv[2])

    # get GCL from config.json
    config = json.load(open("pyconfig.json"))
    GCL = config["GCL"]

    # initialize gdp package
    print "Initializing gdp..."
    gdp.gdp_init()

    # converts the GCL common name to its 256-bit name
    gcl_name = gdp.GDP_NAME(GCL)

    # open the GCL for reading
    gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_RO)
    
    # read num records starting from record number recno
    print("Reading record numbers %d to %d from %s...\n" % (recno, recno+num-1, GCL))
    gcl_handle.multiread(recno, num)

    # results from multiread are returned one by one as events
    while True:
        event = gcl_handle.get_next_event(None)

        if event["type"] == gdp.GDP_EVENT_DATA:
            print("Record number: %d" % event["datum"]["recno"])
            print("Data: %s" % event["datum"]["data"]) 
            print("Event datum (record): %s\n" % event["datum"])
        elif event["type"] == gdp.GDP_EVENT_EOS:
            # handles the event when the multiread finishes
            print("###### END %s ######\n" % os.path.basename(__file__))
            break
        else:
            # handles other events
            break
    
except (IndexError, ValueError):
    print "Usage: specify the starting record number <recno> and the number of records to read <num>"
    print "    $: python gdp_multiread.py <recno> <num>"
    pass
