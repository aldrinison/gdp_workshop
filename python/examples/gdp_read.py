import gdp
import sys
import json
import os

print("\n##### START %s #####\n" % os.path.basename(__file__))

# parse command line argument for recno
try:
    recno = int(sys.argv[1])

    # get GCL from config.json
    config = json.load(open("pyconfig.json"))
    GCL = config["GCL"]

    # initialize gdp package
    print "Initializing gdp..."
    gdp.gdp_init()

    # converts the GCL common name to its 256-bit name
    gcl_name = gdp.GDP_NAME(GCL)

    # open the GCL for reading
    gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_RO)
  
    # read record based on recno
    print("Reading record number %d from %s...\n" % (recno, GCL))
    result = gcl_handle.read(recno)
    print("Record number: %d" % result["recno"])
    print("Data: %s" % result["data"])
    print("Record: %s" % result)

    print("\n###### END %s ######\n" % os.path.basename(__file__))
except (IndexError, ValueError):
    print "Usage: specify the record number <recno>"
    print "    $: python gdp_read.py <recno>"
    pass
