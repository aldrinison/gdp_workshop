import gdp
import json
import os
import gdpapp
import time

print("\n##### START %s #####\n" % os.path.basename(__file__))

# get GCL and base URL of app from config.json
config = json.load(open("pyconfig.json"))
GCL = config["GCL"]
baseurl = config["baseurl"]

# initialize gdp package
print "Initializing gdp..."
gdp.gdp_init()

# converts the GCL common name to its 256-bit name
gcl_name = gdp.GDP_NAME(GCL)

# open the GCL for reading
gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_RO)

# subscribe to the GCL
print("Subscribing to GCL %s:\n" % GCL)
gcl_handle.subscribe(0, 0, None)
print "start: ", time.time()

# event handler for GCL subscription
while True:
    # blocks operation by waiting for event
    event = gcl_handle.get_next_event(None)

    if event["type"] == gdp.GDP_EVENT_DATA:
        data = event["datum"]["data"]
        ts = event["datum"]["ts"]["tv_sec"]

        print("Record number: %d" % event["datum"]["recno"])
        print("Data: %s" % data)
        print("Timestamp: %d" % ts)

        try:
            tuple = gdpapp.send(baseurl, GCL, str(data), str(ts))
            print("Sent request to %s" % tuple[0])
            print("Status: %d\n" % tuple[1])
        except gdpapp.ConnectionError:
            print("Connection refused. Check if the server is running.\n")
            pass
    elif event["type"] == gdp.GDP_EVENT_EOS:
        # handles the event when subscription ends
        print "end: ", time.time()
        print("\n###### END %s ######\n" % os.path.basename(__file__))
        break
    else:
        break
