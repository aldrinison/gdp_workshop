import gdp
import json
import os

print("\n##### START %s #####\n" % os.path.basename(__file__))

# get GCL from config.json
config = json.load(open("pyconfig.json"))
GCL = config["GCL"]

# initialize gdp package
print "Initializing gdp..."
gdp.gdp_init()

# converts the GCL common name to its 256-bit name
gcl_name = gdp.GDP_NAME(GCL)

# open the GCL for reading
gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_RO)

# subscribe to the GCL
print("Subscribing to GCL %s:\n" % GCL)
gcl_handle.subscribe(0, 0, None)

# event handler for GCL subscription
while True:
    # blocks operation by waiting for event
    event = gcl_handle.get_next_event(None)

    if event["type"] == gdp.GDP_EVENT_DATA:
        print("Record number: %d" % event["datum"]["recno"])
        print("Data: %s" % event["datum"]["data"])
        print("Event datum (record): %s\n" % event["datum"])
    elif event["type"] == gdp.GDP_EVENT_EOS:
        # handles the event when the subscription ends
        print("###### END %s ######\n" % os.path.basename(__file__))
        break
    else:
        # handles other events
        break
