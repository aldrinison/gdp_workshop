import gdp
import random
import time
import json
import os

print("\n##### START %s #####\n" % os.path.basename(__file__))

# get GCL, PEM filenames, and delay from config.json
config = json.load(open("pyconfig.json"))
GCL = config["GCL"]
encrypted_pkey = config["encrypted_pkey"]
unencrypted_pkey = config["unencrypted_pkey"]
delay_sec = config["delay_sec"]

# initialize gdp package
print "Initializing gdp..."
gdp.gdp_init()

# converts the GCL common name to its 256-bit name
gcl_name = gdp.GDP_NAME(GCL)

# only needed when writing to GCL
# PEM file with unencrypted private key can be generated using
# openssl pkey -in <encrypted>.pem -out <unencrypted>.pem
# PEM passphrase is no longer needed for unencrypted private key
skey = gdp.EP_CRYPTO_KEY(filename=unencrypted_pkey, keyform=gdp.EP_CRYPTO_KEYFORM_PEM, flags=gdp.EP_CRYPTO_F_SECRET)

# open the GCL for appending
gcl_handle = gdp.GDP_GCL(gcl_name, gdp.GDP_MODE_AO, {"skey": skey})

# appends random value between 100 and 999 every delay_sec seconds
print("Appending random values between 1 and 1000 every %d seconds to GCL %s:\n" % (delay_sec, GCL))
while True:
    data = random.randint(100, 999)

    try:
        # synchronous append which blocks execution
        # for non-blocking implementation, asynchronous append
        #   can be done with append_async method
        gcl_handle.append({"data": str(data)})
        print("Appended data: %s" % str(data))

        time.sleep(delay_sec)
    except:
        raise
