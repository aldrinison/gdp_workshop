import urllib2

# custom exception
class URLError(Exception):
    pass

class ConnectionError(Exception):
    pass

def send(baseurl, *args):
    url = baseurl
    for arg in args:
        # raise an exception if argument is not a string, or if it contains "/" and " "
        if not isinstance(arg, basestring):
            raise TypeError("Arguments must be a string")
        elif "/" in arg or " " in arg:
            raise URLError("Arguments must not contain '/' and ' '")
        else:
            url = url + "/" + arg

    # sends a HEAD request to the formed URL
    req = urllib2.Request(url)
    req.get_method = lambda : 'HEAD'

    try:
        res = urllib2.urlopen(req)
    
        return url, res.getcode()
    except urllib2.URLError:
        raise ConnectionError
