var express = require("express");
var path = require("path");
var ssechannel = require("sse-channel");
var config = require("./config.json");
// sse-channel usage: https://github.com/rexxars/sse-channel

var app = express();
var router = express.Router();
var dataChannel = new ssechannel();

app.use("/", router);

// use public/ directory as web root
app.use(express.static(__dirname + "/public"));

// set view engine to ejs
app.set("view engine", "ejs");

/*
 *  ROUTES
 */

router.get("/", function(req, res){
  // rendered ejs always referenced from views/ directory
  res.render("index.ejs", {title: "Home"});
});

router.get("/subscribe", function(req, res){
  // adds newly connected client to SSE channel
  dataChannel.addClient(req, res);
  process.stdout.write("Client " + req.connection.remoteAddress + " connected ");
  console.log("(Connections: " + dataChannel.getConnectionCount() + ")");
});

router.get("/publish/:gcl/:data/:ts", publish);

/*
 *  MIDDLEWARE
 */

// data pushed to server, so data will be broadcast to connected clients
function publish(req, res, next){
  var gcl = req.params.gcl;
  var data = req.params.data;
  var ts = req.params.ts;
  conn_count = dataChannel.getConnectionCount();

  console.log("Data received from " + req.connection.remoteAddress + ":");
  console.log("GCL: " + gcl);
  console.log("Data: " + data);
  console.log("Timestamp: " + ts);
  process.stdout.write("Publishing to " + conn_count);
  if (conn_count == 1)
    console.log(" connection\n");
  else
    console.log(" connections\n");

  var json = {
    "gcl": gcl,
    "data": data,
    "ts": ts
  };

  dataChannel.send(JSON.stringify(json));
  res.sendStatus("200");
}

// function is optional
app.listen(config.port, function(){
  console.log("Listening on port " + config.port + "...");
});
