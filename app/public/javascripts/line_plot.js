EVENTURL = "http://localhost:5000/subscribe"

// only need to define event source once
var source = new EventSource(EVENTURL);

// event listener for default (unnamed) message event
source.onmessage = function(event){
  var json_data = JSON.parse(event.data);
  var gcl = json_data.gcl;
  var data = json_data.data;
  var ts = json_data.ts;

  console.log(json_data);
  updatePlot(gcl, data, ts);
};

source.addEventListener("error", function(event){
  console.log("Connection closed");
}, false);

// generate empty plot
Plotly.newPlot(plotDiv, [], {title: "GDP Data"});

// used for mapping a unique GCL to a unique trace index
var gcl_obj = {};

function updatePlot(gcl, data, ts){
  // ref: https://stackoverflow.com/questions/10830357/javascript-toisostring-ignores-timezone-offset
  var tzoffset = new Date().getTimezoneOffset()*60000
  var ts_datetime = new Date(ts*1000 - tzoffset).toISOString().slice(0, -1).replace("T", " ");

  // adds a new trace if no current trace matches with the GCL
  if (gcl_obj[gcl] === undefined){
    Plotly.addTraces(plotDiv, {x: [ts_datetime], y: [data], type: "scatter",name: gcl});
    gcl_obj[gcl] = plotDiv.data.length-1;
    console.log("GCL " + gcl + " mapped to trace index " + gcl_obj[gcl]);
  }
  else{
    
  /*  ref: https://plot.ly/javascript/plotlyjs-function-reference/
   *  Plotly.extendTraces(gd, dataupdate, traceindex)
   *    gd: id of div element containing the plot
   *    dataupdate: object with keys corresponding to the data
   *                  in the traces. Values are an array of arrays,
   *                  with each array corresponding to the data
   *                  to be appended at one trace as specified by the
   *                  index of the trace in traceindex
   *    traceindex: array with indices corresponding to the traces
   *   can also be applied to Plotly.update to replace the whole data array of the traces
   */
  Plotly.extendTraces(plotDiv, {x: [[ts_datetime]], y: [[data]]}, [0]);
  }
}
